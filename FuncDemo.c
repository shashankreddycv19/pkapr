#include<stdio.h>
int main()
{ int p, q, sum;
  printf("Enter two integers: ");
  scanf("%d%d", &p, &q);
  sum=add(p,q);
  display(p, q, sum);
  return 0;
 }
 
 int add(int a, int b)
 { int sum;
   sum=a+b;
   return sum;
 }
 
 void display(int x, int y, int sum)
 { printf("The sum of %d and %d is: %d\n", x, y, sum);
 }